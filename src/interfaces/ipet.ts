export interface IPet {
  donor_id: String,
  name: String,
  age: Number,
  bread: String,
  species: String,
  gender: String
}
