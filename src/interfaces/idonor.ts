export interface IDonor {
  name: String,
  contact: String,
  adress: String,
}
