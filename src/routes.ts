import { Router } from 'express'
import { donorRouter } from './routes/donorRoute'
import { petRouter } from './routes/petRoute'
import { swaggerRouter } from './routes/swaggerRoute'

const routes = Router()

routes.use('/', swaggerRouter)
routes.use('/donor', donorRouter)
routes.use('/pet', petRouter)

export default routes
