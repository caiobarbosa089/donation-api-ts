import { Router } from 'express'
import donorController from '../controllers/donorController'

const donorRouter = Router()

donorRouter.get('/', donorController.getDonor)
donorRouter.post('/', donorController.postDonor)
donorRouter.put('/', donorController.putDonor)
donorRouter.delete('/', donorController.deleteDonor)
donorRouter.get('/all', donorController.getAllDonor)

export { donorRouter }
