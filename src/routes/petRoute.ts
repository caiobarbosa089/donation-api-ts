import { Router } from 'express'
import petController from '../controllers/petController'

const petRouter = Router()

petRouter.get('/', petController.getPet)
petRouter.post('/', petController.postPet)
petRouter.put('/', petController.putPet)
petRouter.delete('/', petController.deletePet)
petRouter.get('/all', petController.getAllPet)
petRouter.get('/donor', petController.getDonorPet)

export { petRouter }
