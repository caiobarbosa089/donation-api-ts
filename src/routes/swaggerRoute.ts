import { Router } from 'express'
import { serve, setup } from 'swagger-ui-express'
const swaggerDocument = require('../swagger/swagger.json')

const swaggerRouter = Router()

swaggerRouter.use('/', serve)
swaggerRouter.get('/', setup(swaggerDocument))

export { swaggerRouter }
