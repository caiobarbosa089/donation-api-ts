import { Document, Schema, Model, model } from 'mongoose'
import { IDonor } from '../interfaces/idonor'

export interface DonorModel extends IDonor, Document { }

const DonorSchema = new Schema({
  name: String,
  contact: String,
  adress: String
}, {
  timestamps: true,
  versionKey: false
})

export const Donor: Model<DonorModel> = model<DonorModel>('donors', DonorSchema)
