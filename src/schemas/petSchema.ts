import { Document, Schema, Model, model } from 'mongoose'
import { IPet } from '../interfaces/ipet'

export interface PetModel extends IPet, Document { }

const PetSchema = new Schema({
  donor_id: String,
  name: String,
  age: Number,
  bread: String,
  species: String,
  gender: String
}, {
  timestamps: true,
  versionKey: false
})

export const Pet: Model<PetModel> = model<PetModel>('pets', PetSchema)
