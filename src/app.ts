import express from 'express'
import routes from './routes'
import mongoose from 'mongoose'

class App {
  app: express.Application

  constructor () {
    this.app = express()
    this.middlewares()
    this.database()
    this.routes()
  }

  /** App config */
  private middlewares (): void {
    this.app.use(express.json())
    this.app.use(express.urlencoded({ extended: true }))
  }

  /** Database config */
  private database (): void {
    // Use this url to connect in local MongoDB
    const url = 'mongodb://root:rootpassword@localhost:27017/donation?authSource=admin'
    // MongoDB (Mongoose) connection
    mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
    // MongoDB (Mongoose) config
    mongoose.set('useFindAndModify', false)
    // Event interceptors if you want to check status
    mongoose.connection.on('connected', () => console.log('\x1b[92m%s\x1b[0m', 'MongoDB (Mongoose) - Connected'))
    mongoose.connection.on('error', error => console.log('\x1b[92m%s\x1b[0m', 'MongoDB (Mongoose) - Error', error))
    mongoose.connection.on('disconnected', () => console.log('\x1b[92m%s\x1b[0m', 'MongoDB (Mongoose) - Disconnected'))
  }

  /** Routes config */
  private routes (): void {
    // All routes
    this.app.use(routes)
    // Return 'Not Found' for any route not mapped
    this.app.use((req, res) => res.status(404).json({ error: 'Not found' }))
  }
}

export default new App().app
