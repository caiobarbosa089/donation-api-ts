import { Request, Response } from 'express'
import { Donor } from '../schemas/donorSchema'
import mongoose from 'mongoose'

class DonorController {
  async getAllDonor (req: Request, res: Response): Promise<Response> {
    try {
      // Pagination filters
      const page = parseInt(req.query?.page as string) || 1
      const limit = parseInt(req.query?.limit as string) || 100
      const skip = (page - 1) * limit
      const pagination = { page, limit, skip }
      // BD connection
      const results = await Donor.find({}, (err, donor) => {
        // Error returned
        if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!' })
        // Invalid data received
        if (!donor) return res.status(400).json({ success: false, error: 'No donors' })
      })
        .skip(skip)
        .limit(limit)
      // BD connection
      // Get total page
      pagination['total'] = await Donor.find({}, (err, donor) => {
        // Error returned
        if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!' })
      }).countDocuments()
      // Everything OK
      return res.status(200).json({ success: true, results, pagination }) // Return collection saved including the MongoDB _id
    } catch (error) {
      // Something wrong
      return res.status(500).send({ success: false, error: error })
    }
  }

  async getDonor (req: Request, res: Response): Promise<Response> {
    try {
      // Query params
      const _id = req.query.id as string || null
      // Invalid params
      if (!_id) return res.status(400).json({ success: false, error: 'No id' })
      // Invalid Id
      if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(400).json({ success: false, error: 'Invalid id' })
      // BD connection
      await Donor.findById(_id, (err, donor) => {
        // Error returned
        if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!' })
        // Invalid data received
        if (!donor) return res.status(400).json({ success: false, error: 'Unauthorized action!' })
        // Everything OK
        return res.status(200).json({ success: true, results: donor }) // Return collection saved including the MongoDB _id
      })
    } catch (error) {
      // Something wrong
      return res.status(500).send({ success: false, error: error })
    }
  }

  async postDonor (req: Request, res: Response): Promise<Response> {
    try {
      // Body
      const donorBody = req.body
      // Invalid data received
      if (Object.keys(donorBody).length === 0) return res.status(400).json({ success: false, error: 'No body' })
      // Body needed
      const { name, contact, adress } = donorBody
      const data = { name, contact, adress }
      // Generating new MongoDB _ID
      const _id = mongoose.Types.ObjectId()
      // BD connection
      await Donor.create({ _id, ...data }, (err, donor) => {
        // Error returned
        if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!', err })
        // Everything OK
        return res.status(201).json({ success: true, results: donor })
      })
    } catch (error) {
      // Something wrong
      return res.status(500).send({ success: false, error: error })
    }
  }

  async putDonor (req: Request, res: Response): Promise<Response> {
    try {
      // Body
      const donorBody = req.body
      // Invalid data received
      if (Object.keys(donorBody).length === 0) return res.status(400).json({ success: false, error: 'No body' })
      // Body needed
      const { _id, name, contact, adress } = donorBody
      const data = { name, contact, adress }
      // Invalid Id
      if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(400).json({ success: false, error: 'Invalid id' })
      // BD connection
      // Check if exists
      const finded = await Donor.findById(_id, (err, donor) => {
        // Error returned
        if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!', err })
      })
      // Id not found
      if (!finded) return res.status(400).json({ success: false, error: 'Id not found' })
      // BD connection
      await Donor.findByIdAndUpdate(_id, { ...data }, { new: true }, (err, donor) => {
        // Error returned
        if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!', err })
        // Everything OK
        return res.status(200).json({ success: true, results: donor })
      })
    } catch (error) {
      // Something wrong
      return res.status(500).send({ success: false, error: error })
    }
  }

  async deleteDonor (req: Request, res: Response): Promise<Response> {
    try {
      // Query params
      // MongoDB _ID
      const _id = req.query.id as string || null
      // Invalid params
      if (!_id) return res.status(400).json({ success: false, error: 'Source required to perform the search!' })
      // Invalid Id
      if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(400).json({ success: false, error: 'Invalid id' })
      // BD connection
      // Check if exists
      const finded = await Donor.findById(_id, (err, donor) => {
        // Error returned
        if (err) return res.status(400).json({ success: false, error: 'Invalid request, something went wrong!', err })
      })
      // Id not found
      if (!finded) return res.status(400).json({ success: false, error: 'Id not found' })
      // Remove donor by it's _ID
      await Donor.deleteOne({ _id }, { }, err => {
        // Something wrong happens
        if (err) return res.status(400).json({ success: false, error: 'Can\'t remove donor!' })
        // Everything OK
        return res.status(200).json({ success: true })
      })
    } catch (error) {
      // Something wrong
      return res.status(500).send({ success: false, error: error })
    }
  }
}

const donorController = new DonorController()
export default donorController
