import app from './app'
const port = 2000

// Default error handler - Detect any error in Express and handle it in middleware
app.use((err, req, res, next) => {
  // Runtime error output in server terminal
  console.error('ERROR ', err)
  // Send generic error message under 500 status response
  res.status(500).json({ success: false, error: 'Something went wrong! Verify the URL you tried to access and if you are filling the parameters correctly if they are needed.' })
})

// Restart app on crash
process.on('uncaughtException', err => { console.error('ERROR ', err) })

// Starting application server listening to it's port
app.listen(port, () => console.log(`Server listening to port ${port}`))
